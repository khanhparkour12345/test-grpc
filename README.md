### Test

## Install protoc

```
- MacOS
brew install protobuf
- Ubuntu
# Make sure you grab the latest version
curl -OL https://github.com/google/protobuf/releases/download/v3.5.1/protoc-3.5.1-linux-x86_64.zip
# Unzip
unzip protoc-3.5.1-linux-x86_64.zip -d protoc3
# Move protoc to /usr/local/bin/
sudo mv protoc3/bin/* /usr/local/bin/
# Move protoc3/include to /usr/local/include/
sudo mv protoc3/include/* /usr/local/include/
# Optional: change owner
sudo chown [user] /usr/local/bin/protoc
sudo chown -R [user] /usr/local/include/google
- Windows
Download the windows archive: https://github.com/google/protobuf/releases
Extract all to C:\proto3
Your directory structure should now be
C:\proto3\bin
C:\proto3\include
Finally, add C:\proto3\bin to your PATH:
```

## Config for client

```
install protoc
npm i -g protoc-gen-grpc-web
cd calculator
protoc -I=. calculator/calculator.proto --js_out=import_style=commonjs,binary:client/my-app/src/lib --grpc-web_out=import_style=commonjs,mode=grpcwebtext:client/my-app/src/lib
--------------------
add
    "@grpc/grpc-js": "^0.3.6",
    "google-protobuf": "^3.7.1",
    "grpc": "^1.19.0",
    "grpc-web": "^1.0.4"
to package.json
--------------------
npm i
npm start
--------------------
```
