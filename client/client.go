// Package main provides ...
package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"test-grpc/calculator"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func main() {
	cc, err := grpc.Dial("18.140.248.198:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Connect error: %v", err)
	}
	defer cc.Close()

	client := calculator.NewCalculatorServiceClient(cc)
	CallSum(client)
	CallPrime(client)
	CallAVG(client)
	FindMax(client)
	CallSquare(client)
	CallSquareTimeout(client)
}

func CallSum(c calculator.CalculatorServiceClient) {
	resp, err := c.Sum(context.Background(), &calculator.SumRequest{
		Num1: 5,
		Num2: 6,
	})
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	fmt.Println("success: ", resp.GetSum())
}

func CallPrime(c calculator.CalculatorServiceClient) {
	stream, err := c.PrimeNumber(context.Background(), &calculator.PNDRequest{
		Number: 120,
	})

	if err != nil {
		log.Fatalf("error: %v", err)
	}
	for {
		resp, err := stream.Recv()

		if err == io.EOF {
			log.Println("server finish streaming")
			return
		}
		if err != nil {
			log.Fatalf("error: %v", err)
		}
		log.Printf("prime number: %v", resp.GetResult())
	}
}

func CallSquareTimeout(c calculator.CalculatorServiceClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Microsecond)
	defer cancel()

	resp, err := c.Square(ctx, &calculator.SquareRequest{
		Num: 10,
	})
	if err != nil {
		log.Fatalf("error: %v", err)
		if errStatus, ok := status.FromError(err); ok {
			fmt.Println("error message: ", errStatus.Message())
			fmt.Println("error code: ", errStatus.Code())
			if errStatus.Code() == codes.InvalidArgument {
				fmt.Println("Invalid agurment")
				return
			}
		}
	}
	fmt.Println("root: ", resp.GetRoot())
}

func CallAVG(c calculator.CalculatorServiceClient) {
	stream, err := c.Average(context.Background())
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	listReq := []calculator.AVGRequest{
		{
			Num: 3,
		},
		{
			Num: 5,
		},
		{
			Num: 1,
		},
		{
			Num: 4,
		},
	}

	for _, req := range listReq {
		time.Sleep(1 * time.Second)
		err := stream.Send(&req)
		if err != nil {
			log.Fatal(err)
		}
	}

	resp, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	fmt.Println(resp.GetResult())
}

func FindMax(c calculator.CalculatorServiceClient) {
	stream, err := c.Max(context.Background())
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	listReq := []calculator.FindMaxRequest{
		{
			Num: 3,
		},
		{
			Num: 4,
		},
		{
			Num: 10,
		},
		{
			Num: 5,
		},
		{
			Num: 20,
		},
		{
			Num: 6,
		},
	}

	waitchannel := make(chan struct{})

	go func() {
		for _, req := range listReq {
			time.Sleep(1 * time.Second)
			err := stream.Send(&req)
			if err != nil {
				log.Fatal(err)
			}
		}
		stream.CloseSend()
	}()
	go func() {
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				log.Println("server finished streaming")
				break
			}
			if err != nil {
				log.Fatalf("error: %v", err)
				break
			}
			log.Printf("max number: %v", resp.GetMax())
		}
		close(waitchannel)
	}()

	<-waitchannel
}

func CallSquare(c calculator.CalculatorServiceClient) {
	resp, err := c.Square(context.Background(), &calculator.SquareRequest{
		Num: -3,
	})
	if err != nil {
		log.Fatalf("error: %v", err)
		if errStatus, ok := status.FromError(err); ok {
			fmt.Println("error message: ", errStatus.Message())
			fmt.Println("error code: ", errStatus.Code())
			if errStatus.Code() == codes.InvalidArgument {
				fmt.Println("Invalid agurment")
				return
			}
		}
	}
	fmt.Println("root: ", resp.GetRoot())
}
